import { Box, Button, Grid, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TablePagination, TableRow, TextField, Typography } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import React, { useState } from 'react';
import { IoMdClose, IoIosSearch } from "react-icons/io";
import { EnhancedTableHead, getComparator, stableSort } from './Resources/EnhancedTableHead';

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1)
    },
    paper: {
        padding: theme.spacing(2)
    },
    tableContainer: {
        maxHeight: '70vh'
    }
}));

const headCells = [
    { id: 'actions', numeric: false, disablePadding: true, label: 'Action' },
    { id: 'chequeno', numeric: true, disablePadding: false, label: 'ChequeNo.' },
    { id: 'amount', numeric: true, disablePadding: false, label: 'Amount' },
    { id: 'supplierref', numeric: true, disablePadding: false, label: 'Suppler' },
    { id: 'chequesof', numeric: true, disablePadding: false, label: 'SOF' },
    { id: 'effectivedate', numeric: true, disablePadding: false, label: 'Effective date' },
    { id: 'duedays', numeric: true, disablePadding: false, label: 'Due Days' },
    { id: 'chequestatus', numeric: true, disablePadding: false, label: 'Status' },
];

const ViewAllChequesToSupplier = () => {
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('');
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(100);

    const [allCheques, setAllCheques] = useState([]);

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };


    return (
        <>
            <Grid container spacing={2}>
                <Grid item xs={12} md={12} lg={12}>
                    <Paper elevation={2} className={classes.paper}>
                        <Grid container spacing={1}>
                            <Grid item md={2}>
                                <Autocomplete
                                    fullWidth
                                    size='small'
                                    id="combo-box-demo"
                                    // value={self?.sofname}
                                    // onChange={(e, newValue) => {
                                    //     setSelf({ ...self, sofname: newValue })
                                    // }}
                                    // options={allBankSOF}
                                    getOptionLabel={(option) => option?.name}
                                    renderInput={(params) => <TextField size='small' {...params} label="SOF" variant="outlined" />}
                                />
                            </Grid>
                            <Grid item md={2}>
                                <Autocomplete
                                    fullWidth
                                    size='small'
                                    id="combo-box-demo"
                                    // value={self?.sofname}
                                    // onChange={(e, newValue) => {
                                    //     setSelf({ ...self, sofname: newValue })
                                    // }}
                                    // options={allBankSOF}
                                    getOptionLabel={(option) => option?.name}
                                    renderInput={(params) => <TextField size='small' {...params} label="Supplier" variant="outlined" />}
                                />
                            </Grid>
                            <Grid item md={2}>
                                {/* select */}
                                <Autocomplete
                                    fullWidth
                                    size='small'
                                    id="combo-box-demo"
                                    // value={self?.sofname}
                                    // onChange={(e, newValue) => {
                                    //     setSelf({ ...self, sofname: newValue })
                                    // }}
                                    // options={allBankSOF}
                                    getOptionLabel={(option) => option?.name}
                                    renderInput={(params) => <TextField size='small' {...params} label="Status" variant="outlined" />}
                                />
                            </Grid>
                            <Grid item md={2} style={{ display: 'flex', alignItems: 'center' }}>
                                <Button variant="contained" color="primary" className={classes.button}>
                                    <IoIosSearch fontSize='1.5rem' />
                                </Button>
                                <Button variant="contained"  >
                                    <IoMdClose fontSize='1.5rem' />
                                </Button>
                            </Grid>


                        </Grid>
                    </Paper>
                </Grid>

                <Grid item xs={12} md={12} lg={12}>
                    {/* <TableView /> */}
                    <Paper >
                        <TableContainer className={classes.tableContainer}>
                            <Table
                                aria-labelledby="tableTitle"
                                size='small'
                                aria-label="enhanced table"
                            >
                                <EnhancedTableHead
                                    order={order}
                                    orderBy={orderBy}
                                    onRequestSort={handleRequestSort}
                                    headCells={headCells}

                                />
                                <TableBody>
                                    {stableSort(allCheques, getComparator(order, orderBy))
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map((row, index) => {
                                            return (
                                                <TableRow
                                                    hover
                                                    tabIndex={-1}
                                                    key={index}

                                                >
                                                    <TableCell component="th" >
                                                        {row.name}
                                                    </TableCell>
                                                    <TableCell align="right">{row.calories}</TableCell>
                                                    <TableCell align="right">{row.fat}</TableCell>
                                                    <TableCell align="right">{row.carbs}</TableCell>
                                                    <TableCell align="right">{row.protein}</TableCell>
                                                    <TableCell align="right">{row.effectiveDate}</TableCell>
                                                    <TableCell align="right">{row.dueDays}</TableCell>
                                                    <TableCell align="right">{row.status}</TableCell>
                                                </TableRow>
                                            );
                                        })}

                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[100, 300, 1000]}
                            component="div"
                            count={allCheques?.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                </Grid>
            </Grid>
        </>
    );
};

export default ViewAllChequesToSupplier;
