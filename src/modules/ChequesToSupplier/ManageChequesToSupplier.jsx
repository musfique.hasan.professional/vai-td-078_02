import React, { useEffect, useState } from 'react';
import { Button, Grid, makeStyles, Paper, Chip, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, Checkbox, IconButton, Box } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import SuccessFailureAlert from './Resources/SuccessFailureAlert';
import { initailSelf } from './DBModels/ChequesForSupplier';
import { getAllSuppliers, getAllBankSOF, alllInvoices } from './APIModels/ChequesForSupplier';
import { PhotoCamera } from '@material-ui/icons';
import { apiBaseUrl } from '../../config';



const viewModeView = "VIEW"
const viewModeNew = "NEW"
const viewModeEdit = "EDIT"
const viewModeDraft = "SAVEASDRAFT"
const viewModeGiveToSupp = "GIVETOSUPP"

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1)
    },
    paper: {
        padding: theme.spacing(2)
    },
}));



const ManageChequesForSupplier = () => {
    const classes = useStyles();
    const [viewMode, setViewMode] = useState(viewModeView)
    const [openSuccessAlert, setOpenSuccessAlert] = useState(false);
    const [openFailureAlert, setOpenFailureAlert] = useState(false);
    const [message, setMessage] = useState("");
    const [allSupplier, setAllSupplier] = useState([]);
    const [allInvoices, setAllInvoices] = useState([]);
    const [allBankSOF, setAllBankSOF] = useState([]);

    /*.............image upload state............ */
    const [selectImage, setSelectImage] = useState('');
    /*......................... */
    const [self, setSelf] = useState(initailSelf)


    const clear = () => {
        setSelf(initailSelf)
        /*.............setting all invoice as a empty array after click on cancel button............ */
        setAllInvoices([])
        /*......................... */
        setViewMode(viewModeView)
        setOpenSuccessAlert(false)
        setOpenFailureAlert(false)
        setMessage("")
    }
    const openSuccess = (message) => {
        setMessage('');
        setOpenSuccessAlert(true);
        setOpenFailureAlert(false);
        setMessage(message);
    }
    const openFailure = (message) => {
        setMessage('');
        setOpenSuccessAlert(false);
        setOpenFailureAlert(true);
        setMessage(message);
    }

    const ValidateDraft = () => {
        if (self.suppliername === null) {
            return "Please Choose Supplier Name";
        }
        return ""
    }
    const ValidateGiveToSupp = () => {
        if (self.suppliername === null) {
            return "Please Choose Supplier Name";
        }
        if (self.sofname === null) {
            return "Please Choose SOF Name";
        }
        if (self.bankname === '') {
            return "Insert Bank Name";
        }
        if (self.accountnumber === '') {
            return "Insert Account Number";
        }
        if (self.chequenumber === '') {
            return "Insert Cheque Number";
        }
        return ""
    }

    const saveCheques = (isDraft) => {
        console.log(viewMode)

        if (isDraft) {
            let err = ValidateDraft();
            if (err !== "") {
                openFailure(err);
                return;
            } else {
                openSuccess('Successfully Draft Created...')
            }
        }
        if (!isDraft) {
            let err = ValidateGiveToSupp();
            if (err !== "") {
                openFailure(err);
                return;
            } else {
                openSuccess('Successfully Supplier Created...')
            }
        }

    }

    const getAllInvoices = (supplierInfo) => {

        const invoicefilter = {
            ...supplierInfo,
            type: "delivery",
            typeisused: true,
            isdone: false,
            isdoneisused: true

        }
        alllInvoices(invoicefilter).then(res => {
            if (res[0]) {
                console.log(res[1]);
                setAllInvoices(res[1])
            } else {
                openFailure(res[1])
            }
        })
    }
    /*.............This Fn is for checkbox to calculate Total amount............ */
    const CheckedInvoice = (id, checked) => {
        let clickedInvoice = allInvoices.find(invoice => invoice._id === id)
        let amount
        if (checked) {
            amount = (self.amount + clickedInvoice.total)
        } else if (!checked) {
            amount = (self.amount - clickedInvoice.total)
        }
        setSelf({ ...self, amount: amount >= 0 ? (amount) : 0 })
    }
    /*......................... */

    // encode product image to base64
    const encodeFileBase64 = (file) => {
        var reader = new FileReader();
        if (file) {
            const lastDot = file.name.lastIndexOf(".");
            const ext = file.name.substring(lastDot + 1);
            reader.readAsDataURL(file);
            reader.onload = () => {
                var Base64 = reader.result;
                const data = { ...self, img64: Base64, imgext: ext }
                setSelf(data)
            };
        }
    };
    useEffect(() => {
        const supplierFilter = {
            iscustomer: false,
            iscustomerisused: true,
            status: true,
            statusisused: true
        }
        getAllSuppliers(supplierFilter)
            .then(res => {
                if (res[0]) {
                    console.log(res[1]);
                    setAllSupplier(res[1])
                } else {
                    openFailure(res[1])
                }
            })
        const sofFilter = {
            type: "bank",
            typeisused: true,
            status: true,
            statusisused: true
        }
        getAllBankSOF(sofFilter)
            .then(res => {
                if (res[0]) {
                    // console.log(res[1]);
                    setAllBankSOF(res[1])
                } else {
                    openFailure(res[1])
                }
            })
    }, [])


    return (
        <Grid container spacing={2}>
            <SuccessFailureAlert
                openSuccessAlert={openSuccessAlert}
                message={message}
                openFailureAlert={openFailureAlert}
                setopenSuccessAlert={setOpenSuccessAlert}
                setopenFailureAlert={setOpenFailureAlert}
            />
            <Grid item xs={12} md={12} lg={12}>
                <Button
                    size='small'
                    onClick={() => setViewMode(viewModeNew)}
                    disabled={viewMode !== viewModeView}
                    className={classes.button}
                    variant="contained" color="secondary">
                    Create New
                </Button>
                <Button size='small' color="secondary"
                    onClick={() => setViewMode(viewModeDraft)}
                    variant="contained"
                    className={classes.button}
                    disabled={viewMode !== viewModeEdit}
                >
                    Edit
                </Button>
                <Button
                    onClick={() => {
                        saveCheques(true)
                    }}
                    size='small' color="primary"
                    variant="contained"

                    className={classes.button}
                    disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                >
                    Save as Draft
                </Button>
                <Button
                    onClick={() => {
                        saveCheques(false)
                    }}
                    size='small' color="primary"
                    variant="contained"
                    className={classes.button}
                    disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                >
                    Give to Supplier
                </Button>
                <Button size='small'
                    onClick={() => {
                        clear()
                    }}
                    variant="contained">
                    Cancel
                </Button>
            </Grid>
            <Grid item xs={12} md={12} lg={12}>
                <Paper elevation={2} className={classes.paper}>
                    <Grid container spacing={2}>
                        <Grid item xs={6} md={6} lg={4}>
                            <Autocomplete
                                disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                fullWidth
                                size='small'
                                id="combo-box-demo"
                                value={self?.suppliername}
                                onChange={(e, newValue) => {
                                    /*..getting all invoices and for empty field amount will be zero.. */
                                    setSelf({ ...self, suppliername: newValue, amount: 0 })
                                    getAllInvoices({ supplierref: newValue?._id, supplierrefisused: true, })
                                }}
                                options={allSupplier}
                                getOptionLabel={(option) => option?.name}
                                renderInput={(params) => <TextField size='small' {...params} label="Choose Supplier Name" variant="outlined" />}
                            />

                        </Grid>
                        <Grid item xs={6} md={6} lg={8}></Grid>
                        <Grid item xs={12} md={12} lg={12}>
                            <Paper>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table" size='small'>
                                        <TableHead style={{ backgroundColor: "#aaa" }}>
                                            <TableRow>
                                                <TableCell ><b>Select</b></TableCell>
                                                <TableCell align="right"><b>Invoice Number</b></TableCell>
                                                <TableCell align="right"><b>Invoice Date</b></TableCell>
                                                <TableCell align="right"><b>Total</b></TableCell>
                                                <TableCell align="right"><b>Status</b></TableCell>
                                                <TableCell align="right"><b>Paid</b></TableCell>
                                                <TableCell align="right"><b>Returned Amount</b></TableCell>
                                                <TableCell align="right"><b>Remaining</b></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                allInvoices.map(invoice => (
                                                    <TableRow hover key={invoice?._id}>
                                                        <TableCell >
                                                            <Checkbox
                                                                onChange={(e) => {
                                                                    /*...passing id & checkbox status to calculate Total amount... */
                                                                    CheckedInvoice(invoice?._id, e.target.checked)
                                                                }}
                                                                color="primary"
                                                                inputProps={{ 'aria-label': 'secondary checkbox' }}
                                                            />
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            {/*.............invoice number............ */}
                                                            {invoice?.serial}

                                                        </TableCell>
                                                        <TableCell align="right">
                                                            {/*.............date............ */}
                                                            {new Date(invoice?.date).toLocaleDateString()}
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            { /*.............total amount............ */}
                                                            {invoice?.total}
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            < Chip
                                                                size='small'
                                                                style={{ backgroundColor: "red", color: "white" }}
                                                                /*.............status............ */
                                                                label={invoice?.status}
                                                            />
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            { /*.............paid amount............ */}
                                                            {invoice?.paidamount ? invoice?.paidamount : 0}
                                                        </TableCell>
                                                        <TableCell align="right">0</TableCell>
                                                        <TableCell align="right">
                                                            {/*.............remaining amount............ */}
                                                            {invoice?.total - invoice?.paidamount >= 0 ?
                                                                invoice?.total - invoice?.paidamount :
                                                                invoice?.total
                                                            }</TableCell>
                                                    </TableRow>
                                                ))
                                            }

                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Paper>
                        </Grid>
                        <Grid item xs={8} md={8} lg={10}></Grid>
                        <Grid item xs={4} md={4} lg={2}>
                            <TextField
                                value={(self?.amount).toFixed(1)}
                                InputProps={{
                                    readOnly: true,
                                }}
                                disabled={viewMode === viewModeView || viewMode === viewModeEdit} fullWidth type="number" size='small' label="Total" variant="outlined" />
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
            <Grid item xs={12} md={12} lg={12}>
                <Paper elevation={2} className={classes.paper}>
                    <Typography gutterBottom><b>Cheque Details</b></Typography>
                    <Grid container spacing={2}>
                        <Grid item md={4}>
                            <Autocomplete
                                disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                fullWidth
                                size='small'
                                id="combo-box-demo"
                                value={self?.sofname}
                                onChange={(e, newValue) => {
                                    /*.............SOF............ */
                                    if (newValue) {
                                        setSelf({ ...self, sofname: newValue, bankname: newValue?.bankname, accountnumber: newValue?.bankaccountnumber })
                                    } else {
                                        setSelf({ ...self, sofname: newValue, bankname: '', accountnumber: '' })
                                    }
                                }}
                                options={allBankSOF}
                                getOptionLabel={(option) => option?.name}
                                renderInput={(params) => <TextField size='small' {...params} label="SOF" variant="outlined" />}
                            />
                        </Grid>
                        <Grid item md={4}>
                            <TextField
                                value={self?.bankname}
                                InputProps={{
                                    readOnly: true,
                                }}
                                disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                fullWidth size='small' id="outlined-basic" label="Bank Name" variant="outlined" />
                        </Grid>
                        <Grid item md={4}>
                            <TextField
                                value={self?.accountnumber}
                                InputProps={{
                                    readOnly: true,
                                }}
                                disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                fullWidth size='small' id="outlined-basic" label="Account Number" variant="outlined" />
                        </Grid>
                        <Grid item md={4}>
                            <TextField
                                value={self?.chequenumber}
                                // onChange={(e) => {
                                //     setSelf({ ...self, chequenumber: e.target.value })
                                // }}

                                disabled={viewMode === viewModeView || viewMode === viewModeEdit} fullWidth size='small' id="outlined-basic" label="Cheque Number" variant="outlined" />
                        </Grid>
                        <Grid item md={2}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker

                                    // value={comparisonData.datefrom}
                                    // onChange={(date) => {
                                    //     if (!isNaN(date?.getTime())) {
                                    //         setComparisonData({ ...comparisonData, datefrom: date })
                                    //     }
                                    // }}
                                    size='small'
                                    autoOk
                                    variant="inline"
                                    inputVariant="outlined"
                                    label="Effective Date"
                                    format="MM/dd/yyyy"
                                    disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
            {/*.............This Grid for upload image............ */}
            <Grid item xs={12} md={12} lg={12}>
                <Paper elevation={2} className={classes.paper}>
                    <Grid container spacing={2}>
                        <Grid item xs={6} md={2} lg={2}>
                            <Box>
                                <label htmlFor="icon-button-file">
                                    <input
                                        onChange={(e) => {
                                            setSelectImage(e.target.files[0])
                                            encodeFileBase64(e.target.files[0])
                                        }}
                                        style={{ display: 'none' }}
                                        accept="image/*"
                                        id="icon-button-file"
                                        type="file"

                                    />
                                    <IconButton color="primary" aria-label="upload picture" component="span"
                                        disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                    >
                                        <PhotoCamera />
                                    </IconButton>
                                </label>
                            </Box>
                        </Grid>
                        <Grid item xs={6} md={10} lg={10}>
                            <Box textAlign='center'>
                                {
                                    (self?.imgpath && selectImage === '') ?
                                        <img src={apiBaseUrl + '/' + self?.imgpath} width='50%' height='500px' alt="" visibility='visible' /> :
                                        selectImage && <img src={URL.createObjectURL(selectImage)} width='50%' height='500px' alt="" visibility='visible' />
                                }
                            </Box>
                        </Grid>

                    </Grid>
                </Paper>
            </Grid>
            { /*......................... */}

        </Grid>
    );
};

export default ManageChequesForSupplier;