const initailSelf = {
    _id: '',
    supplierref: '',
    suppliername: null,
    amount: 0,
    sofref: '',
    sofname: null,
    bankname: '',
    accountnumber: '',
    chequenumber: '',
    effectivedate: new Date(),
    chequestate:'',
    imgpath: '',
    img64: '',
    imgtext: ''
}

export { initailSelf }