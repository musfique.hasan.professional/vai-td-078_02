import axios from "axios";
import { apiBaseUrl } from "../../../config";

const getAllSuppliers = async (filter) => {

    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/contact/get_all`,
            headers: {
                "Content-Type": "application/json",
            },
            data: JSON.stringify(filter)
        });
        // console.log(response);
        return [true, response.data.result];
    } catch (error) {
        return [false, error];
    }


}
const getAllBankSOF = async (filter) => {

    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/sof/get_all_populated`,
            headers: {
                "Content-Type": "application/json",
            },
            data: JSON.stringify(filter),
        });
        // console.log(response);
        return [true, response.data.result];
    } catch (error) {
        return [false, error];
    }

}
const alllInvoices = async (filter) => {

    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/purchasing/get_all_populated`,
            headers: {
                "Content-Type": "application/json",
            },
            data: JSON.stringify(filter),
        });
        // console.log(response);
        return [true, response.data.result];
    } catch (error) {
        return [false, error];
    }

}

export { getAllSuppliers, getAllBankSOF, alllInvoices }