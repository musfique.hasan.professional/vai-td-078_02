import React from 'react';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { makeStyles } from '@material-ui/core/styles';

function descendingComparator(a, b, orderBy) {
    if (typeof b[orderBy] === "object" && typeof a[orderBy] === "object") {
        if (b[orderBy]?.name?.toLowerCase() < a[orderBy]?.name?.toLowerCase()) {
            return -1;
        } else if (b[orderBy]?.name?.toLowerCase() > a[orderBy]?.name?.toLowerCase()) {
            return 1
        } else {
            return 0
        }
    }
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
}
  
function getComparator(order, orderBy) {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
}
  
function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    margin: theme.spacing(1)
},
visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
},
container: {
    maxHeight: "60vh",
},
iconButtons: {
    marginRight: "0.5rem"
}
}));

function EnhancedTableHead(props) {
    const classes = useStyles();
    const { order, orderBy, onRequestSort, headCells } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
                {headCells.map((headCell) => (
                    headCell.id === "actions" ? <TableCell style={{ backgroundColor: "#aaa" }}><b>Actions</b></TableCell> :
                            headCell.id === "applicableforfcs" ? <TableCell align='right' style={{ backgroundColor: "#aaa" }}><b>Applicable For FCs</b></TableCell> :
                            headCell.id === "analyst" ? <TableCell align='right' style={{ backgroundColor: "#aaa" }}><b>Analyst</b></TableCell> :
                            headCell.id === "externalreference" ? <TableCell align='right'><b>External Reference</b></TableCell> :
                            headCell.id === "description" ? <TableCell style={{ backgroundColor: "#aaa" }} align='right'><b>Description</b></TableCell> :
                                headCell.id === "gcas" ? <TableCell style={{ backgroundColor: "#aaa" }} align='right'><b>GCAS</b></TableCell> :
                                headCell.id === "chequesstatus" ? <TableCell style={{ backgroundColor: "#aaa" }} align='right'><b>Status</b></TableCell> :
                                headCell.id === "returninginvoicerefs" ? <TableCell style={{ backgroundColor: "#aaa" }} align='right'><b>Returned Amount</b></TableCell> :
                                headCell.id === "chequesremaining" ? <TableCell style={{ backgroundColor: "#aaa" }} align='right'><b>Remaining</b></TableCell> :
                                headCell.id === "paidamount" ? <TableCell style={{ backgroundColor: "#aaa" }} align='right'><b>Paid</b></TableCell> :
                                <TableCell
                                    style={{ backgroundColor: "#aaa" }}
                                    key={headCell.id}
                                    align='right'
                                    sortDirection={orderBy === headCell.id ? order : false}
                                >
                                    <TableSortLabel
                                        active={orderBy === headCell.id}
                                        direction={orderBy === headCell.id ? order : 'asc'}
                                        onClick={createSortHandler(headCell.id)}
                                    >
                                        <b>{headCell.label}</b>
                                        {orderBy === headCell.id ? (
                                            <span className={classes.visuallyHidden}>
                                                {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                            </span>
                                        ) : null}
                                    </TableSortLabel>
                                </TableCell>
                ))}
            </TableRow>
      </TableHead>
    );
}
  
EnhancedTableHead.propTypes = {
classes: PropTypes.object.isRequired,
numSelected: PropTypes.number.isRequired,
onRequestSort: PropTypes.func.isRequired,
onSelectAllClick: PropTypes.func.isRequired,
order: PropTypes.oneOf(['asc', 'desc']).isRequired,
orderBy: PropTypes.string.isRequired,
rowCount: PropTypes.number.isRequired,
};


export {
    EnhancedTableHead,
    getComparator,
    stableSort 
}