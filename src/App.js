
import ChequesToSupplierContainer from './modules/ChequesToSupplier/ChequesToSupplierContainer';

function App() {
  return (
    <div className="">

      <ChequesToSupplierContainer />
    </div>
  );
}

export default App;
